//
//  ContentTableViewCell.h
//  MersKorea
//
//  Created by coozplz on 2015. 6. 5..
//  Copyright (c) 2015년 Coozplz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContentTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *authorLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;

@end
