//
//  WebViewController.m
//  MersKorea
//
//  Created by coozplz on 2015. 6. 5..
//  Copyright (c) 2015년 Coozplz. All rights reserved.
//

#import "GPSViewController.h"
#import "AppDelegate.h"
#import <GoogleMaps/GoogleMaps.h>

@interface GPSViewController ()


@end

@implementation GPSViewController {
    GMSMapView *mapView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    mapView = [GMSMapView mapWithFrame:CGRectZero camera:nil];

    [mapView animateToLocation:CLLocationCoordinate2DMake(37.2000, 128.0286)];
    [mapView animateToZoom:7];
    mapView.myLocationEnabled = YES;
    self.view = mapView;
    [self fetchingPatientLocation];

}


- (void) fetchingPatientLocation {
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@", SERVER_URL, @"loadPatientLocation"]];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response,
                                   NSData *data, NSError *connectionError) {
                               if (data.length > 0 && connectionError == nil) {
                                   NSArray *locationData = [NSJSONSerialization JSONObjectWithData:data
                                                                                            options:0
                                                                                              error:NULL];


                                   NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                                   [dateFormatter setDateFormat:@"yyyy-MM-dd 'at' HH:mm"];

                                   NSString *dateString = [dateFormatter stringFromDate:[NSDate date]];
                                   self.title = [NSString stringWithFormat:@"%@ [%lu]", dateString, [locationData count]];

                                   for (NSDictionary *dic in locationData) {
                                        NSString *title = dic[@"desc"];
                                       float lat = [dic[@"lat"] floatValue];
                                       float lng = [dic[@"lng"] floatValue];
                                       [self createGMSMarker:title lng:lng lat:lat];

                                   }
                               }
                           }];
}


/**
* 지도에 표시할 마킹 정보를 생성한다.
*
* @param    title   제목
* @param    lng     경도
* @param    lat     위도
*
*/
- (void) createGMSMarker : (NSString *) title lng:(float)lng lat:(float) lat {
    GMSMarker *marker = [[GMSMarker alloc] init];
    [marker setTitle:title];
    marker.position = CLLocationCoordinate2DMake(lat, lng);
    marker.map = mapView;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
@end
