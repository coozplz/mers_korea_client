//
//  RSSViewController.m
//  MersKorea
//
//  Created by coozplz on 2015. 6. 5..
//  Copyright (c) 2015년 Coozplz. All rights reserved.
//

#import <GoogleMobileAds/GoogleMobileAds.h>
#import "RSSViewController.h"
#import "ContentTableViewCell.h"
#import "RssDetailViewController.h"
#import "AppDelegate.h"
#import "MMProgressHUD.h"
#import "MLAlertViewUtil.h"

@interface RSSViewController ()
@property NSArray *rssData ;


@end

@implementation RSSViewController {
@private
    NSDictionary *selectedRowData;
    UIRefreshControl *refreshControl;

}

- (void)viewDidLoad {

    [super viewDidLoad];
    refreshControl = [[UIRefreshControl alloc] init];

    CGRect screenSize = [UIScreen mainScreen].bounds;
    self.bannerView = [[GADBannerView alloc] initWithAdSize:kGADAdSizeBanner];
    self.bannerView.frame = CGRectMake(0, 0, screenSize.size.width, kGADAdSizeBanner.size.height);

    self.bannerView.adUnitID = @"ca-app-pub-7047713338014061/9099306436";
    self.bannerView.rootViewController = self;
    [self.view  addSubview:self.bannerView];
    self.rssTableView.frame = CGRectMake(0, kGADAdSizeBanner.size.height, screenSize.size.width, screenSize.size.height - kGADAdSizeBanner.size.height);

    GADRequest *request = [GADRequest request];


    [self.bannerView loadRequest:request];

    [refreshControl addTarget:self action:@selector(refreshData:) forControlEvents:UIControlEventValueChanged];
    [self.rssTableView addSubview:refreshControl];

    [MMProgressHUD setPresentationStyle:MMProgressHUDPresentationStyleFade];
    [MMProgressHUD showWithTitle:nil status:@"Loading..." cancelBlock:^{

    }];

    [self requestRssData];

}


- (void) requestRssData {
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@", SERVER_URL, @"loadRssData"]];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response,
                                   NSData *data, NSError *connectionError) {
                               [MMProgressHUD dismiss];
                               [refreshControl endRefreshing];
                               if (data.length <= 0 || connectionError) {
                                   MLAlertWithTitle(@"오류", @"네트워크 데이터를 조회하지 못하였습니다.");
                                   return ;
                               }


                               NSArray *receivedData = [NSJSONSerialization JSONObjectWithData:data
                                                                                       options:0
                                                                                         error:NULL];

                               NSSortDescriptor *timeSort = [[NSSortDescriptor alloc] initWithKey:@"time" ascending:NO];

                               NSMutableArray *sortArray = [@[timeSort] mutableCopy];

                               self.rssData = receivedData;
                               [self.rssData sortedArrayUsingDescriptors:sortArray];

                               [self.rssTableView reloadData];
                           }];

}

- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [super viewWillAppear:animated];
}


- (void)viewWillDisappear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    [super viewWillDisappear:animated];

}


- (void) refreshData : (UIRefreshControl *) refreshControl {
    [self requestRssData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"segue_table_to_web"]) {
        NSIndexPath *indexPath = [self.rssTableView indexPathForSelectedRow];

        NSDictionary *dict = self.rssData[(NSUInteger) indexPath.row];
        RssDetailViewController *viewController = [segue destinationViewController];

        NSString *link = dict[@"link"];
        viewController.linkUrl = link;
    }
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {


    return [self.rssData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    static NSString *cellIdentifier = @"rss_cell_identifier";
    ContentTableViewCell *cell = [self.rssTableView dequeueReusableCellWithIdentifier:cellIdentifier];

    if (cell == nil) {

        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ContentTableViewCell" owner:self options:nil];
        cell = nib[0];
        NSDictionary *dict = self.rssData[(NSUInteger) indexPath.row];

        cell.titleLabel.text = dict[@"title"];
        cell.authorLabel.text = dict[@"author"];
        cell.dateLabel.text = dict[@"time"];
    }
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    selectedRowData = self.rssData[(NSUInteger) indexPath.row];
    [self performSegueWithIdentifier:@"segue_table_to_web" sender:self];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {

    return 85;
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft|| interfaceOrientation  ==UIInterfaceOrientationLandscapeRight);
}

@end
