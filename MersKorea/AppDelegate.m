//
//  AppDelegate.m
//  MersKorea
//
//  Created by coozplz on 2015. 6. 5..
//  Copyright (c) 2015년 Coozplz. All rights reserved.
//

#import "AppDelegate.h"
#import <GoogleMaps/GMSServices.h>




@interface AppDelegate ()
@property CIContext *context;

@end

@implementation AppDelegate



- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
//
    if ([UIAlertController class]) {
        //
        // iOS 8에서 오류가 발생되어 아래와 같이 context를 생성한다.
        // BSXPCMessage received error for message
        //
        self.context = [CIContext contextWithOptions:@{kCIContextUseSoftwareRenderer: @(NO)}];
    }
    [GMSServices provideAPIKey:@"AIzaSyCVhpXAEk_Rbs5H94q5H7Ss_N8Hc7nFVc4"];

    // Override point for customization after application launch.
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
