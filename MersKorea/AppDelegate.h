//
//  AppDelegate.h
//  MersKorea
//
//  Created by coozplz on 2015. 6. 5..
//  Copyright (c) 2015년 Coozplz. All rights reserved.
//

#import <UIKit/UIKit.h>
#define SERVER_URL @"http://www.venturebridge.co.kr:9991/"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) UITabBarController *tabBarController;

@end

