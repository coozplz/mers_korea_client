//
//  MLAlertViewUtil.m
//  tcat4i
//
//  Created by YoungJoon Chun on 2/10/14.
//  Copyright (c) 2014 Chun, YoungJoon. All rights reserved.
//

#import "MLAlertViewUtil.h"


@interface MLAlertDelegate : NSObject <UIAlertViewDelegate> {
@private
    MLAlertDelegateBlock alertDelegate;
    UIAlertView* alertview;
    // prevent ARC from release self.
    id selfObject;
}
@property (readonly) UIAlertView* alertview;

- initWithTitle:(NSString*) title
              msg:(NSString*) msg
            block:(MLAlertDelegateBlock) block
cancelButtonTitle:(NSString*)cancelButtonTitle
otherButtonTitles:(NSArray*) otherButtonTiles;

@end

@implementation MLAlertDelegate
@synthesize alertview;

- initWithTitle:(NSString*) title
              msg:(NSString*) msg
            block:(MLAlertDelegateBlock) block
cancelButtonTitle:(NSString*)cancelButtonTitle
otherButtonTitles:(NSArray*) otherButtonTitles
{
    self = [super init];

    alertDelegate = [block copy];
    alertview = [[UIAlertView alloc] initWithTitle:title
                                           message:msg
                                          delegate:self
                                 cancelButtonTitle:cancelButtonTitle
                                 otherButtonTitles:nil];

    for(NSString* s in otherButtonTitles)
    {
        [alertview addButtonWithTitle:s];
    }

    selfObject = self; // to be released in alert delegate...

    [alertview show];

    return self;
}

- (void) dealloc {
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    alertDelegate(alertView, buttonIndex);
    selfObject = nil;
}

@end

UIAlertView* MLAlertWithBlock(NSString* title,
        NSString* msg,
        MLAlertDelegateBlock delegateBlock,
        NSString* cancelButtonTitle,
        NSString* otherButtonTitles, ... )
{
    NSMutableArray *buttonsArray = [NSMutableArray array];

    if (otherButtonTitles)
    {
        [buttonsArray addObject:otherButtonTitles];

        NSString* btnTitle;
        va_list argumentList;
        va_start(argumentList, otherButtonTitles);
        while((btnTitle = va_arg(argumentList, NSString *)))
        {
            [buttonsArray addObject: btnTitle];
        }
        va_end(argumentList);
    }

    @autoreleasepool {
        MLAlertDelegate* alertd = [[MLAlertDelegate alloc] initWithTitle:title
                                                                     msg:msg
                                                                   block:delegateBlock
                                                       cancelButtonTitle:cancelButtonTitle
                                                       otherButtonTitles:buttonsArray];
        return alertd.alertview;
    }
}

#pragma mark -


UIAlertView* MLAlertWithTitle(NSString* title, NSString* message, ...)
{
    va_list ap;
    va_start(ap, message);
    NSString* format = [[NSString alloc] initWithFormat:message arguments:ap];
    va_end(ap);

    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:title
                                                    message:format delegate:nil
                                          cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                          otherButtonTitles:nil];
    if([NSThread isMainThread])
    {
        [alert show];
    }
    else {
        [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:NO];
    }

    return alert;
}



